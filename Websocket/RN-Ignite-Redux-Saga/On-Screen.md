to use the websocket subscription,

1. set the states in your React Native screen :
```
constructor (props){
    ...
    this.state = {
        ...
        ...,
        wsMutable : {
            field1: 0,
            field2: 'lorem'
        }
    }
}
```

2. use mapStateToProps to access redux-governed states that changes through triggering by the reducers
```
const mapStateToProps = (state) => {
  return {
    ...
    wsMutable: state.{entityReducer}.{entityState}
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(MapScreen)
```

3. subscribe to the websocket on Mount
```
...
componentWillMount () {
    ...
    WebsocketService.connect()
    WebsocketService.subscribeToLocation()
    WebsocketService.subscribeToSpeed()
    ...
  }

  componentWillUnmount () {
    WebsocketService.disconnect()
  }
...
```
disconnect usually already includes unsubscribing to the websocket, so no need to unsubscribe first (unless you want to be sure).
by subscribing you will have access to the redux-states that are websocket-mutable (see `processWebsocketMessage` in [Handler](./Handler.md) for details)

4. handle the received data on props receive

assume there is a little something to do before you can accept the reducer's changes, you can add some conditionals before setting the new state.

here is an example where you check the current device before setting the state, so only websocket messages containing data related the current device focused will affect the component's state fields
```
componentWillReceiveProps (newProps) {

    ...

    if (newProps.wsMutable){
        const { deviceId } = newProps.wsMutable
        // check if deviceId is
        // the same as the device that is currently focused
        if(id === this.state.currentDevice){
            const { field1, field2 } = newProps.entityReducerName

            this.setState({
                wsMutable : {
                    field1,
                    field2
                }
            })
        }
    }
    ...

  }
```

5. final render steps
```
render() {
    { wsMutable } = this.state
    return <View>
    ...
    ...
    <View style={styles.realtimeData}>
        <Text>{wsMutable.field1} {wsMutable.field2}</Text>
    </View>

    </View>
}
```
==================================

in the example above, when a websocket message that looks like so :
```
{
    field1: 736,
    field2: 'ipsum'
}
```
is received, it will render "736 ipsum" on screen

===================================
