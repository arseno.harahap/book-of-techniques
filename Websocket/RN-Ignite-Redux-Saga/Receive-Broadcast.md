
Welcome to this websocket-receiving tutorial for apps based on React-Native + Redux-Saga

we assume you already have knowledge of this tech stack :
1. [React](https://reactjs.org/)
2. [React Native](https://facebook.github.io/react-native/)
3. [Redux-Saga](https://github.com/redux-saga/redux-saga)

================================


Steps to receive Websocket broadcasts on React-Native :
1. [set up a subscription for the websocket](./Setup-Subscription.md)
2. [set up the handler to do something upon receiving](./Handler.md)
3. [using the websocket in the screen](./On-Screen.md)
