websocket handler in a Ignite-Jhipster with Redux-Saga is usually coded in `yourApp/app/shared/websockets/websocket.sagas.js`

```
// processes the websocket message
export function * processWebsocketMessage ({ subscription, msg }) {

  console.log(`WS-Saga: ${subscription}`)
  console.log(msg)
  console.log('=======')

  switch (subscription) {
    case '{entityName0}':
      yield select(selectState, '{entityName0}', '{entity0Field}')
      new{entityName0} = msg.{entityName0};
      yield put({EntityName0}Actions.{entityName0}Success(new{EntityName0}))
      break
    case '{entityName1}':
      yield select(selectState, '{entityName1}', '{entity1Field}')
      newSpeed = msg.{entityName1};
      yield put({EntityName1}Actions.{entityName1}Success(new{EntityName1}))
      break
    default:
      console.tron.log(`Uncaught subscription: ${subscription}`)
      console.tron.log(`Uncaught message: ${msg}`)
  }

}
```

the number of cases will depend on how many websockets are there that is going to be used by the app

the actions which are used in the Saga-function `put` (like `yield put(someActions.anAction)`) is located in the respective entities' reducer. you can also use your own reducer if you don't want to use ignite-jhipster's generated reducers that are generated according to the entities

in the reducers, be sure to check the state-mutator lambda functions ( usually looks like so : )
```
// successful api lookup for single entity
export const success = (state, action) => {
  const { entityField } = action
  return state.merge({
    fetchingOne: false,
    errorOne: null,
    entityField
  })
}
```
 we're going to use, along with the paired Types, because that is what affects the screen later when we use the redux-managed states
