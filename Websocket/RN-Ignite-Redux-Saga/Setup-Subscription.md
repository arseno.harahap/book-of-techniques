To set up subscription, go to the file `yourApp/app/shared/websockets/websocket.service.js`

in there you will need to set up
- subscribers
```
let someSubscriber //usually follows the pattern "[entityName]Subscriber"
let chatSubscriber
```
- subscription starter function
```
function subscribeTo{EntityName} (){
    if (!subscriptions.hasOwnProperty('{entityName}')){
    subscriptions.{entityName} = { subscribed: true }
    connection.then(() => {
      {entityName}Subscriber = stompClient.subscribe({pathToSubscription}, onMessage.bind(this, 'entityName'))
    })
  } else {
    console.log('Already subscribed to Location');
  }
}
```
- subscription stopper function
```
function unsubscribe{EntityName} () {
  delete subscriptions.{entityName}
  if (someSubscriber){
    someSubscriber.unsubscribe()
  }
}
```

variables to observe :

- {pathToSubscription} : consult with your backend, usually it's `/topic/websocket/{entityName}`. you can search for codes within your backend framework project that has websocket. on Spring-based projects, just search codes that has `simpleMessagingTemplate.convertAndSend("` in the code
- {entityName} : consult with your JHipster project manager/devlead (usually the guy who manages the Jhipster entities)
