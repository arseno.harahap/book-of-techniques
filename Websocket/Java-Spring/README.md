Steps to enable and use Websocket broadcasting on Java-Spring :
1. [Initiate websocket service](./Initiate-Websocket.md)
2. [Broadcast message through websocket](./Broadcast-Websocket.md)
